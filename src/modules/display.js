// Displays the normal and stretched camera feed

import fx from 'glfx';
import { createEl, addEl } from 'lmnt';
import autoBind from 'auto-bind';
import { padding } from '../config.json';
import { lerp } from './utils';

export default class Display {
  constructor(state, camera) {
    autoBind(this);

    this.state = state;
    this.camera = camera;
    this.el = createEl('div', { className: 'display' });

    this.pinTarget = 0;
    this.pinAmount = 0;

    this.debugCanvas = createEl('canvas', { className: 'display-canvas' });
    this.debugContext = this.debugCanvas.getContext('2d');
    this.debugContext.font = 'bold 100px serif';
    this.debugContext.textAlign = 'center';
    this.canvas = fx.canvas();
    this.canvas.style.opacity = 0;
    this.canvas.className = 'display-canvas main';
    addEl(this.el, this.debugCanvas, this.canvas);
  }

  update() {
    this.debugContext.putImageData(this.camera.stack[this.camera.stack.length - this.state.delay - 1], 0, 0);

    Object.keys(this.state.corners).forEach((corner) => {
      const { x, y, age } = this.state.corners[corner];
      const color = `rgb(${Math.min(Math.max((age * 3), 0), 255)}, ${255 - Math.min(Math.max((age * 3), 0), 255)}, 0)`;
      this.debugContext.fillStyle = color;
      this.debugContext.fillText(corner.toUpperCase(), x, y);
    });

    const actual_x1 = this.state.corners.tl.x;
    const actual_y1 = this.state.corners.tl.y;
    const actual_x2 = this.state.corners.tr.x;
    const actual_y2 = this.state.corners.tr.y;
    const actual_x3 = this.state.corners.br.x;
    const actual_y3 = this.state.corners.br.y;
    const actual_x4 = this.state.corners.bl.x;
    const actual_y4 = this.state.corners.bl.y;

    const target_x1 = padding;
    const target_y1 = padding;
    const target_x2 = this.state.cameraWidth - padding;
    const target_y2 = padding;
    const target_x3 = this.state.cameraWidth - padding;
    const target_y3 = this.state.cameraHeight - padding;
    const target_x4 = padding;
    const target_y4 = this.state.cameraHeight - padding;

    const current_x1 = lerp(actual_x1, target_x1, this.pinAmount);
    const current_y1 = lerp(actual_y1, target_y1, this.pinAmount);
    const current_x2 = lerp(actual_x2, target_x2, this.pinAmount);
    const current_y2 = lerp(actual_y2, target_y2, this.pinAmount);
    const current_x3 = lerp(actual_x3, target_x3, this.pinAmount);
    const current_y3 = lerp(actual_y3, target_y3, this.pinAmount);
    const current_x4 = lerp(actual_x4, target_x4, this.pinAmount);
    const current_y4 = lerp(actual_y4, target_y4, this.pinAmount);

    // TODO: get delayed image working (currently not sized correct)
    // const texture = this.canvas.texture(this.camera.delayedCanvas); // TODO: maybe render this in display instead of camera?
    const texture = this.canvas.texture(this.camera.video);
    this.canvas.draw(texture).perspective(
      [actual_x1, actual_y1, actual_x2, actual_y2, actual_x3, actual_y3, actual_x4, actual_y4],
      [current_x1, current_y1, current_x2, current_y2, current_x3, current_y3, current_x4, current_y4],
    ).update();

    this.pinAmount += (this.pinTarget - this.pinAmount) / 10;

    requestAnimationFrame(this.update);
  }

  show() {
    this.el.classList.remove('hidden');
  }

  hide() {
    this.el.classList.add('hidden');
  }

  pin() {
    this.pinTarget = 1;
    this.canvas.style.opacity = 1;
    if (this.unpinTimeout) {
      clearTimeout(this.unpinTimeout);
      this.unpinTimeout = null;
    }
  }

  unpin() {
    this.pinTarget = 0;
    this.unpinTimeout = setTimeout(() => { this.canvas.style.opacity = 0; }, 1000);
  }

  resize() {
    const scaleX = window.innerWidth / this.state.cameraWidth;
    const scaleY = window.innerHeight / this.state.cameraHeight;
    this.canvas.style.transform = `scale(${scaleX}, ${scaleY})`;

    this.debugCanvas.width = this.state.cameraWidth;
    this.debugCanvas.height = this.state.cameraHeight;
    this.debugCanvas.style.transform = `scale(${scaleX}, ${scaleY})`;
  }
}
