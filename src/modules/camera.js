import { createEl } from 'lmnt';
import autoBind from 'auto-bind';
import { camConstraints } from '../config.json';

export default class Camera {
  constructor(state) {
    this.state = state;
    autoBind(this);

    this.video = createEl('video', { }, { autoplay: true, muted: true, playsinline: true });
    this.canvas = createEl('canvas');
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
    this.context = this.canvas.getContext('2d');

    this.delayedCanvas = createEl('canvas');
    this.delayedCanvas.width = window.innerWidth;
    this.delayedCanvas.height = window.innerHeight;
    this.delayedContext = this.delayedCanvas.getContext('2d');
    this.stack = Array(100);
  }

  start() {
    window.navigator.mediaDevices.getUserMedia(camConstraints)
      .then((stream) => {
        const { width, height } = stream.getVideoTracks()[0].getSettings();
        this.state.cameraWidth = width;
        this.state.cameraHeight = height;
        this.video.srcObject = stream;
        this.video.style.width = window.innerWidth;
        this.video.style.height = window.innerHeight;
        this.video.addEventListener('loadedmetadata', () => {
          this.video.play();
          this.state.cameraStarted = true;
        });
      })
      .catch((err) => {
        console.log('camera error', err);
      });
  }

  stop() {
    this.video.srcObject.getTracks().forEach((track) => { track.stop(); });
    this.state.cameraStarted = false;
  }

  getData() {
    this.context.drawImage(this.video, 0, 0, this.state.cameraWidth, this.state.cameraHeight);
    const imageData = this.context.getImageData(0, 0, this.state.cameraWidth, this.state.cameraHeight);

    this.stack.push(imageData);
    this.stack.shift();

    if (this.stack[this.stack.length - this.state.delay - 1]) {
      this.delayedContext.putImageData(this.stack[this.stack.length - this.state.delay - 1], 0, 0);
    }

    return imageData;
  }

  resize() {
    this.canvas.width = this.state.landscape ? this.state.cameraWidth : this.state.cameraHeight;
    this.canvas.height = this.state.landscape ? this.state.cameraHeight : this.state.cameraWidth;
    this.delayedCanvas.width = this.state.landscape ? this.state.cameraWidth : this.state.cameraHeight;
    this.delayedCanvas.height = this.state.landscape ? this.state.cameraHeight : this.state.cameraWidth;

    const screenAspect = this.state.fullscreen ? window.screen.width / window.screen.height : window.innerWidth / window.innerHeight;
    const cameraAspect = this.state.cameraWidth / this.state.cameraHeight;
    const scale = screenAspect < cameraAspect
    // ? this.state.fullscreen
      ? window.innerWidth / this.state.cameraWidth
      : window.innerHeight / this.state.cameraHeight;

    this.canvas.style.transform = `translate(-50%, -50%) scale(${scale})`;
    this.delayedCanvas.style.transform = `translate(-50%, -50%) scale(${scale})`;
  }
}
