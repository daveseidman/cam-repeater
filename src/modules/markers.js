// Displays Aruco markers in the corners of the screen

import ArucoMarker from 'aruco-marker';
import { createEl, addEl } from 'lmnt';
import autoBind from 'auto-bind';

export default class Markers {
  constructor(state) {
    this.state = state;
    autoBind(this);
    this.width = window.innerWidth;
    this.height = window.innerHeight;

    this.el = createEl('div', { className: 'markers hidden hidden2' });
    this.topLeft = createEl('div', { className: 'markers-marker top left', innerHTML: new ArucoMarker(100).toSVG('500px') });
    this.topRight = createEl('div', { className: 'markers-marker top right', innerHTML: new ArucoMarker(200).toSVG('500px') });
    this.bottomRight = createEl('div', { className: 'markers-marker bottom right', innerHTML: new ArucoMarker(300).toSVG('500px') });
    this.bottomLeft = createEl('div', { className: 'markers-marker bottom left', innerHTML: new ArucoMarker(400).toSVG('500px') });

    addEl(this.el, this.topLeft, this.topRight, this.bottomLeft, this.bottomRight);
    window.addEventListener('resize', this.resize);
  }

  show() {
    this.el.classList.remove('hidden');
  }

  hide() {
    this.el.classList.add('hidden');
  }

  resize() {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
  }

  update() {
    this.topLeft = createEl('div', { className: 'markers-marker top left', innerHTML: new ArucoMarker(100 + this.state.delay).toSVG('500px') });
    addEl(this.el, this.topLeft);
  }
}
