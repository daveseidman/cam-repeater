// Tutorial video of app

import { createEl, addEl } from 'lmnt';

export default class Tutorial {
  constructor(state) {
    this.state = state;
    this.el = createEl('div', { className: 'tutorial hidden' }, {}, { click: ({ target }) => { if (target === this.el) this.state.tutorial = false; } });
    this.video = createEl('div', { className: 'tutorial-video' });
    this.videoElement = createEl('video', { className: 'tutorial-video-player', src: 'assets/videos/tutorial.mp4' }, { controls: true });
    this.closeButton = createEl('button', { className: 'tutorial-video-close', innerText: '×' }, {}, { click: () => { this.state.tutorial = false; } });
    addEl(this.video, this.videoElement, this.closeButton);
    addEl(this.el, this.video);
  }

  show() {
    this.el.classList.remove('hidden');
    this.videoElement.play();
  }

  hide() {
    this.el.classList.add('hidden');
    this.videoElement.pause();
  }
}
