// Various utility functions

module.exports.distance = (p1, p2) => Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));

module.exports.lerp = (v0, v1, t) => v0 * (1 - t) + v1 * t;

module.exports.openFullscreen = () => {
  const _element = document.documentElement;
  if (_element.requestFullscreen) {
    _element.requestFullscreen();
  } else {
    if (_element.mozRequestFullScreen) {
      _element.mozRequestFullScreen();
    } else {
      if (_element.webkitRequestFullscreen) {
        _element.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
      }
    }
  }
};
