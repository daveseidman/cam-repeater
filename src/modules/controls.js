// Display main controls / menu

import { createEl, addEl } from 'lmnt';

export default class Controls {
  constructor(state) {
    this.state = state;

    this.el = createEl('div', { className: 'controls hidden' });

    this.main = createEl('div', { className: 'controls-main' });
    this.selectorLabel = createEl('p', { className: 'controls-main-selector-label', innerText: 'select screen type:' });
    this.selector = createEl('radio', { className: 'controls-main-selector slider', name: 'screenType', values: ['-', 'First Screen', 'Cam Repeater'], value: '-' }, {}, { change: ({ target }) => { this.state.camera = target.value === 'Cam Repeater'; } });

    this.stretchButton = createEl('button', { className: 'controls-main-stretch', innerText: 'Stretch' }, {}, { click: () => { this.state.pinning = !this.state.pinning; this.stretchButton.innerText = this.state.pinning ? 'Reset' : 'Stretch'; } });
    this.delay = createEl('div', { className: 'controls-main-delay' });
    this.delayLabel = createEl('p', { className: 'controls-main-delay-label', innerText: 'Delay: 0' });
    this.reduceDelayButton = createEl('button', { className: 'controls-main-delay-down', innerText: '-' }, {}, { click: () => { this.state.delay -= 1; } });
    this.increaseDelayButton = createEl('button', { className: 'controls-main-delay-up', innerText: '+' }, {}, { click: () => { this.state.delay += 1; } });
    addEl(this.delay, this.reduceDelayButton, this.delayLabel, this.increaseDelayButton);

    addEl(this.main, this.selectorLabel, this.selector, this.stretchButton, this.delay);

    this.tutorialButton = createEl('button', { className: 'controls-tutorial', innerText: 'tutorial' }, {}, { click: () => { this.state.tutorial = true; } });
    this.creditsButton = createEl('button', { className: 'controls-credits', innerText: '?' }, {}, { click: () => { this.state.credits = !this.state.credits; } });

    addEl(this.el, this.main, this.tutorialButton, this.creditsButton);
  }

  show() {
    this.el.classList.remove('hidden');
  }

  hide() {
    this.el.classList.add('hidden');
  }

  update() {
    this.delayLabel.innerText = `Delay: ${this.state.delay}`;
  }
}
