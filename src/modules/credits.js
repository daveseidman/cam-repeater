// Display credits overlay

import { addEl, createEl } from 'lmnt';

export default class Credits {
  constructor(state) {
    this.state = state;

    this.el = createEl('div', { className: 'credits hidden' }, {}, { click: ({ target }) => { if (target === this.el) this.state.credits = false; } });
    this.content = createEl('div', { className: 'credits-content' });
    this.whatIsTitle = createEl('h1', { className: 'credits-content-title', innerText: 'CamRepeater.com' });
    this.whatIsText = createEl('p', { className: 'credits-content-text',
      innerHTML:
      'CamRepeater.com is a visual experiment by <a target=\'_blank\' href=\'http://daveseidman.com\'>dave seidman</a>.<br><br>'
      + 'It is intended to be opened on a device with a rear-facing camera and used to repeat what the camera sees as large as possible on it\'s display.<br><br>'
      + 'By repeating multiple devices one can create a chained periscope. Further testing proved that a circular array of CamRepeater\'s can be used to trap light in a feedback loop as seen <a target=\'_blank\' href=\'http://twitter.com\'>here</a>.<br><br>'
      + 'For best results, try different lighting conditions and adjusting each screens brightness. The image will blur progressively but it\'s possible to normalize the brightness by adjusting the screens one at a time.  Add-to-homescreen will enable fullscreen mode.<br><br>'
      + 'checkout <a target=\'blank\' href=\'gitlab.com\'>Source Code</a> | print <a target=\'blank\' href=\'assets/images/test-pattern.jpg\'>test pattern</a>',
    });

    this.closeButton = createEl('button', { className: 'credits-content-close', innerText: '×' }, {}, { click: () => { this.state.credits = false; } });

    addEl(this.content, this.whatIsTitle, this.whatIsText, this.closeButton);
    addEl(this.el, this.content);
  }

  show() {
    this.el.classList.remove('hidden');
  }

  hide() {
    this.el.classList.add('hidden');
  }
}
