// Detect position and age of markers in the camera stream

import { AR } from 'js-aruco';
import autoBind from 'auto-bind';
import { createEl, addEl } from 'lmnt';

export default class Detector {
  constructor(state, camera) {
    autoBind(this);

    this.state = state;
    this.camera = camera;

    this.detector = new AR.Detector();
    this.el = createEl('div', { className: 'detector' });
    this.debug = createEl('p', { className: 'detector-debug' });

    this.stopped = false;

    addEl(this.el, this.debug);
  }

  start() {
    this.stopped = false;
    this.update();
  }

  stop() {
    this.stopped = true;
  }

  update() {
    const data = this.camera.getData();
    this.markers = this.detector.detect(data);

    this.markers.forEach((marker) => {
      const { corners } = marker;
      const x = corners.reduce((acc, cur) => acc + cur.x, 0) / 4;
      const y = corners.reduce((acc, cur) => acc + cur.y, 0) / 4;
      marker.x = x;
      marker.y = y;
      // marker.id = id;
      delete marker.corners;
    });
    const corners = {
      tl: this.markers.filter(marker => marker.id >= 100 && marker.id <= 199)[0],
      tr: this.markers.filter(marker => marker.id === 200)[0],
      br: this.markers.filter(marker => marker.id === 300)[0],
      bl: this.markers.filter(marker => marker.id === 400)[0],
    };

    this.combinedAge = this.state.tracking ? this.combinedAge : 0;
    Object.keys(corners).forEach((corner) => {
      if (corners[corner]) {
        this.state.corners[corner].x = corners[corner].x;
        this.state.corners[corner].y = corners[corner].y;
        this.state.corners[corner].age = 0;
        this.state.corners[corner].id = corners[corner].id;
        if (corner === 'tl') this.state.delay = corners[corner].id - 100;
      } else {
        this.state.corners[corner].age += 1;
        this.combinedAge += 1;
      }
    });
    if (this.combinedAge === 0 && !this.state.tracking) this.state.tracking = true;
    if (this.state.tracking && this.combinedAge > 300) this.state.tracking = false;

    if (!this.stopped) requestAnimationFrame(this.update);
  }
}
