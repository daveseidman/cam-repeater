import './index.scss';
import { createEl, addEl } from 'lmnt';
import gtag, { install } from 'ga-gtag';
import onChange from 'on-change';
import autoBind from 'auto-bind';
import Camera from './modules/camera';
import Detector from './modules/detector';
import Markers from './modules/markers';
import Display from './modules/display';
import Controls from './modules/controls';
import Tutorial from './modules/tutorial';
import Credits from './modules/credits';

import { openFullscreen } from './modules/utils';

class App {
  constructor() {
    autoBind(this);
    const state = {
      cameraStarted: false,
      camera: null,
      landscape: window.orientation === undefined || window.orientation === 90,
      width: window.innerWidth,
      height: window.innerHeight,
      cameraWidth: 0,
      cameraHeight: 0,
      controls: false,
      tutorial: false,
      tracking: false,
      pinning: false, // whether to show image corner pinned or not
      lock: false, // whether to update perspective or not (not yet implemented)
      fullscreen: false,
      delay: 0,
      corners: {
        tl: { x: 0, y: 0, age: 0 },
        tr: { x: window.innerWidth, y: 0, age: 0 },
        br: { x: window.innerWidth, y: window.innerHeight, age: 0 },
        bl: { x: 0, y: window.innerHeight, age: 0 },
      },
    };

    this.state = onChange(state, this.update, { ignoreKeys: ['corners'] });
    this.el = createEl('div', { className: 'app' });

    addEl(this.el);

    setTimeout(() => {
      this.markers.el.classList.remove('hidden2');
      this.state.controls = true;
    }, 3000);

    this.camera = new Camera(this.state);
    this.detector = new Detector(this.state, this.camera);
    this.markers = new Markers(this.state);
    this.display = new Display(this.state, this.camera);
    this.controls = new Controls(this.state);
    this.logo = createEl('img', { className: 'logo', src: 'assets/images/camrepeater-logo.png' });
    this.tutorial = new Tutorial(this.state);
    this.credits = new Credits(this.state);

    addEl(this.el, this.detector.el, this.display.el, this.logo, this.markers.el, this.controls.el, this.credits.el, this.tutorial.el);

    setTimeout(() => {
      document.body.addEventListener('click', this.showControls);
      document.body.addEventListener('mousemove', this.showControls);
    }, 3000);
    window.addEventListener('resize', this.resize);

    install('G-WGT09SZLE3');
  }

  update(path, current, previous) {
    console.log(path, previous, current);

    if (path === 'controls') {
      this.controls[current ? 'show' : 'hide']();
    }

    if (path === 'tutorial') {
      this.tutorial[current ? 'show' : 'hide']();
      if (current) gtag('config', 'G-WGT09SZLE3', { page_title: 'Tutorial', page_path: '/Tutorial' });
    }

    if (path === 'credits') {
      this.credits[current ? 'show' : 'hide']();
      if (current) gtag('config', 'G-WGT09SZLE3', { page_title: 'Credits', page_path: '/Credits' });
    }

    if (path === 'camera') {
      if (current) {
        this.camera.start();
        this.display.show();
        this.el.className = 'app repeat';
        gtag('config', 'G-WGT09SZLE3', { page_title: 'Start Camera', page_path: '/StartCam' });
      } else {
        this.markers.show();
        this.display.hide();
        this.el.className = 'app first';
        if (previous) this.camera.stop();
        gtag('config', 'G-WGT09SZLE3', { page_title: 'First Screen', page_path: '/FirstScreen' });
      }
    }

    if (path === 'cameraStarted' && current) {
      if (current) {
        gtag('config', 'G-WGT09SZLE3', { page_title: 'Camera Started', page_path: '/StartCam/Started' });
        this.detector.start();
        this.display.update();
        if (window.location.hostname !== 'localhost') openFullscreen();
      } else {
        this.detector.stop();
      }
    }

    if (path === 'cameraWidth' || path === 'cameraHeight') {
      this.display.resize();
    }

    // TODO: should pinning also STOP tracking?
    if (path === 'pinning') {
      this.display[current ? 'pin' : 'unpin']();
      this.markers[current ? 'show' : 'hide']();
      this.el.classList[current ? 'add' : 'remove']('expanded');
    }

    if (path === 'delay') {
      this.markers.update();
      this.controls.update();
    }
  }

  showControls() {
    this.state.controls = true;
    if (this.controlsTimeout) clearTimeout(this.controlsTimeout);

    this.controlsTimeout = setTimeout(() => { this.state.controls = false; }, 3000);
  }

  resize() {
    this.display.resize();
    this.camera.resize();
  }
}
const app = new App();
if (window.location.hostname === 'localhost') window.app = app;
